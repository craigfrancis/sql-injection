<?php

//--------------------------------------------------
// Config

	$html = [];

//--------------------------------------------------
// No Escaping

	$url = ($_GET['url'] ?? '/ onerror=alert(1)');
	$url = $_GET['url']; // sonarcloud to see as tainted

	$html[] = '<img src=' . $url . ' alt="" />';

//--------------------------------------------------
// Missing Quotes

	$url = ($_GET['url'] ?? '/ onerror=alert(1)');
	$url = $_GET['url']; // sonarcloud to see as tainted

	$html[] = "<img src=" . htmlentities($url) . " alt='' />";

//--------------------------------------------------
// Bad Escaping
// htmlentities() doesn't encode single quotes by default.
// I got this fixed for PHP 8.1
// https://github.com/php/php-src/commit/50eca61f68815005f3b0f808578cc1ce3b4297f0

	$url = ($_GET['url'] ?? "/' onerror='alert(1)");
	$url = $_GET['url']; // sonarcloud to see as tainted

	$html[] = "<img src='" . htmlentities($url) . "' alt='' />";

//--------------------------------------------------
// Context Issue

	$url = ($_GET['url'] ?? 'javascript:alert(1);');
	$url = $_GET['url']; // sonarcloud to see as tainted

	$html[] = '<a href="' . htmlentities($url) . '">Link</a>';

//--------------------------------------------------
// Context Issue
// The browsers HTML parser is not aware of escaped JavaScript strings.

	$url = ($_GET['url'] ?? '</script><script>alert(1)</script>');
	$url = $_GET['url']; // sonarcloud to see as tainted

	$html[] = '<script> var url = "' . addslashes($url) . '"; </script>';

//--------------------------------------------------
// Incomplete Escaping (ref urlencode)

	$name = ($_GET['name'] ?? 'A&B');
	$name = $_GET['name']; // sonarcloud to see as tainted

	$html[] = '<a href="./?name=' . htmlentities($name) . '">Link</a>';

//--------------------------------------------------
// Encoding
// PHP just assumes the string is UTF-8 (since 5.4).
// Without a charset declaration, the browser will also guess at the encoding.
// For example, this classic UTF-7 value for Internet Explorer.

	$value = ($_GET['value'] ?? '+ADw-script+AD4-alert(1)+ADw-+AC8-script+AD4-');
	$value = $_GET['value']; // sonarcloud to see as tainted

	header('Content-Type: text/html; charset=');
	header('X-Content-Type-Options: -');

	$html[] = '<p>' . htmlentities($value) . '</p>';

//--------------------------------------------------
// Print

	foreach ($html as $h) {
		echo $h . "\n\n";
	}

?>