<?php

$db = new mysqli('localhost', 'test', 'test', 'test');

$id = ($_POST['id'] ?? 1);
$id = $_POST['id']; // So sonarcloud can see it as tainted, really?

$queries = [
		['SELECT *, 1 FROM user WHERE id = "' . $id . '"'                                  , []], // Clearly Insecure
		['SELECT *, 2 FROM user WHERE id = "' . intval($id) . '"'                          , []],
		['SELECT *, 3 FROM user WHERE id = "' . mysqli_real_escape_string($db, $id) . '"'  , []],
		['SELECT *, 4 FROM user WHERE id =  ' . mysqli_real_escape_string($db, $id)        , []], // Insecure due to the missing quotes, e.g. 'id' would result in 'id = id'
		['SELECT *, 5 FROM user WHERE id =  ?'                                             , [$id]],
	];

foreach ($queries as $query) {

	list($sql, $parameters) = $query;

	echo $sql . "\n";

	$statement = $db->prepare($sql);
	if (!empty($parameters)) {
		$ref_types = '';
		foreach ($parameters as $key => $value) {
			$ref_types .= (is_int($value) ? 'i' : 's'); // 'd' for double, or 'b' for blob.
			$ref_values[] = &$parameters[$key];
		}
		if (count($ref_values) != $statement->param_count) {
			throw new mysqli_sql_exception('Invalid parameter count', 2034);
		} else {
			array_unshift($ref_values, $ref_types);
			call_user_func_array(array($statement, 'bind_param'), $ref_values);
		}
	}
	$statement->execute();
	$result = $statement->get_result();
	while ($row = mysqli_fetch_assoc($result)) {
		print_r($row);
	}

	echo "\n";

}

$query = "SELECT * FROM users WHERE id = '" . $id . "'"; // Unsafe example from documentation (trying to trigger at least 1 warning)
$result = mysqli_query($db, $query);

$query = "SELECT * FROM users WHERE id = " . mysqli_real_escape_string($db, $id); // Maybe sonarcloud does not work with arrays?
$result = mysqli_query($db, $query);

?>